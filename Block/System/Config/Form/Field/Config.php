﻿<?php
/**
 * Copyright © 2016-2017 IAGC. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace IAGC\AdvancedShippingSetting\Block\System\Config\Form\Field;

class Config extends \IAGC\AdvancedSettingCore\Block\System\Config\Form\Field\Config
{
    protected function getFullscreenTitle(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return __("Advanced Shipping Configuration");
    }

    protected function getHelpUrl(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->getUrl('iagc_advancedshippingsetting/help/display');
    }

    protected function getToolbarContent(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return parent::getToolbarContent($element);
    }

    protected function getFooterContent(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '';
    }
}
