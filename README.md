﻿# Description

Advanced Shipping Module for Magento 2

## Add-ons for Advanced Shipping

You can find [add-ons for Advanced Shipping on IAGC Store](https://en.store.iagc.com/magento2-module-advanced-shipping.html).

## Installation

:warning: _Please note that you can only install the extension using composer._

* Backup your store database and web directory
* Open a terminal and move to Magento root directory
* Run these commands in your terminal

```shell
# You must be in Magento root directory
composer require iagc/magento2-module-advanced-shipping-setting:^2.5.0
php bin/magento cache:clean
php bin/magento module:enable IAGC_AdvancedSettingCore
php bin/magento module:enable IAGC_AdvancedShippingSetting
php bin/magento setup:upgrade
# Execute setup:di:compile only if the store is in production mode
php bin/magento setup:di:compile
```

* If you are logged to Magento backend, logout from Magento backend and login again

## Documentation

[See the documentation](https://iagc.com/doc/en/magento2-module-advanced-shipping)

## License

Copyright © 2016-2019 IAGC. All rights reserved.

No warranty, explicit or implicit, provided.

Files can not be copied and/or distributed without the express permission of IAGC.


Icons:

https://fortawesome.github.io/Font-Awesome/

## Contributing

By contributing to this project, you grant a world-wide, royalty-free, perpetual, irrevocable, non-exclusive, transferable license to all users under the terms of the license(s) under which this project is distributed.
